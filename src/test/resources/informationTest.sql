
SET IDENTITY_INSERT agent ON;
INSERT INTO agent (id,codigo, nombre, apellido, anose, cdetransito, via) values (10012,'PR001', 'JUAN','RODRIGUEZ', '2.0', 'EF103', NULL);
INSERT INTO agent (id,codigo, nombre, apellido, anose, cdetransito, via) values (10013,'PR002', 'BRAYAN','RAMIREZ', '4.5', 'EF143', NULL);
SET IDENTITY_INSERT agent OFF;
SET IDENTITY_INSERT via ON;
INSERT INTO via (id, Tipo, Sentido, Numero, Ncongestion) VALUES (10013, 'Autopista', 'Calle', 0, '0.00'),(10014, 'Autopista', 'Calle', 12, '12.00'),(10015, 'Autopista', 'Calle', 34, '68.00'),(10016, 'Carretera Principal', 'Calle', 133, '46.50');
SET IDENTITY_INSERT via OFF;

SET IDENTITY_INSERT information ON;
INSERT INTO information (id,cagente, direccion, nagente, via) values (50004,'PR001', 'Calle34','JUAN', 10015 );
SET IDENTITY_INSERT information OFF;