SET IDENTITY_INSERT agent ON;
INSERT INTO agent (id,codigo, nombre, apellido, anose, cdetransito, via) values (10012,'PR001', 'JUAN','RODRIGUEZ', '2.0', 'EF103', NULL);
INSERT INTO agent (id,codigo, nombre, apellido, anose, cdetransito, via) values (10013,'PR002', 'BRAYAN','RAMIREZ', '4.5', 'EF143', NULL);
SET IDENTITY_INSERT agent OFF;
SET IDENTITY_INSERT via ON;
INSERT INTO via (Id, Tipo, Sentido, Numero, Ncongestion) VALUES (10016,'Autopista','Calle',12,40.00);
SET IDENTITY_INSERT via OFF;