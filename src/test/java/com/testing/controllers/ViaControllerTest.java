//package com.testing.controllers;
//
//import com.testing.dto.ViaDTO;
//import com.testing.mapper.ViaRowMapper;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.boot.test.web.client.TestRestTemplate;
//import org.springframework.http.HttpEntity;
//import org.springframework.http.ResponseEntity;
//import org.springframework.jdbc.core.BeanPropertyRowMapper;
//import org.springframework.jdbc.core.JdbcTemplate;
//import org.springframework.test.context.jdbc.Sql;
//import java.util.Arrays;
//import java.util.List;
//
//import static org.junit.jupiter.api.Assertions.*;
//
//@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
//class ViaControllerTest {
//
//    @Autowired
//    private TestRestTemplate testRestTemplate;
//
//    @Autowired
//    private JdbcTemplate jdbcTemplate;
//
//    @Test
//    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "/viaTest.sql")
//    @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = "/testDelete.sql")
//    void getAll() {
//        BeanPropertyRowMapper beanPropertyRowMapper = new BeanPropertyRowMapper(ViaDTO.class);
//        ViaDTO[] via = testRestTemplate.getForObject("/api/Via", ViaDTO[].class);
//        List<ViaDTO> viaList = Arrays.asList(via);
//        assertNotNull(viaList);
//        String sql = "Select * from via";
//        List<ViaDTO> Via = jdbcTemplate.query(sql,beanPropertyRowMapper);
//        for(int i = 0; i < Via.size(); i++) {
//            assertEquals(Via.get(i).getId(),viaList.get(i).getId());
//        }
//    }
//
//
//
//    @Test
//    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "/viaTest.sql")
//    @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = "/testDelete.sql")
//    void getOne() {
//        ViaDTO viaDTO = testRestTemplate.getForObject("/api/Via/10013",ViaDTO.class);
//        assertNotNull(viaDTO);
//        String sql = "Select * from via where id = ?";
//        ViaDTO Via = jdbcTemplate.queryForObject(sql,new ViaRowMapper(), viaDTO.getId());
//        assertEquals(viaDTO,Via);
//
//    }
//
//    @Test
//    void save() {
//        jdbcTemplate.execute("SET IDENTITY_INSERT via OFF;");
//        ViaDTO via = new ViaDTO();
//        via.setType("Carretera principal");
//        via.setSense("Carrera");
//        via.setNumber(121);
//        via.setCongestionNumber(13.44f);
//        HttpEntity<ViaDTO> request = new HttpEntity<>(via);
//        ResponseEntity<ViaDTO> viaDTOResponseEntity = testRestTemplate.postForEntity("/api/Via", request , ViaDTO.class );
//        assertNotNull(viaDTOResponseEntity.getBody().getId());
//        String sql = "Select * from via where id = ?";
//        assertNotNull(jdbcTemplate.queryForObject(sql, new ViaRowMapper(), viaDTOResponseEntity.getBody().getId()));
//        assertEquals("Carretera principal", viaDTOResponseEntity.getBody().getType());
//        assertEquals("Carrera", viaDTOResponseEntity.getBody().getSense());
//        assertEquals(121, viaDTOResponseEntity.getBody().getNumber());
//        sql = "delete from via where id = ?";
//        jdbcTemplate.update(sql,viaDTOResponseEntity.getBody().getId());
//    }
//
//    @Test
//    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "/viaTest.sql")
//    @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = "/testDelete.sql")
//    void update() {
//        ViaDTO viaDTO = jdbcTemplate.queryForObject("Select * from via where id = ? ", new ViaRowMapper(), 10013l);
//        viaDTO.setSense("Carrera");
//        HttpEntity<ViaDTO> request = new HttpEntity<>(viaDTO);
//        testRestTemplate.put("/api/Via/"+viaDTO.getId(), request);
//        viaDTO = jdbcTemplate.queryForObject("Select * from via where id = ? ", new ViaRowMapper(), 10013l);
//        assertNotEquals("Calle",viaDTO.getSense());
//    }
//
//    @Test
//    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "/viaTest.sql")
//    @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = "/testDelete.sql")
//    void delete() {
//        boolean rpa = false;
//        ViaDTO viaDTO = jdbcTemplate.queryForObject("Select * from via where id = ? ", new ViaRowMapper(), 10013l);
//        String url = "/api/Via/"+viaDTO.getId();
//        testRestTemplate.delete(url);
//        try{
//            jdbcTemplate.queryForObject("Select * from via where id = ? ", new ViaRowMapper(), 10013l);
//        }catch (Exception e){
//            rpa = true;
//        }
//        assertTrue(rpa);
//
//    }
//}