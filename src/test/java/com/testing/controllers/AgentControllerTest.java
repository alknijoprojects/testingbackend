//package com.testing.controllers;
//
//import com.testing.dto.AgentDTO;
//import com.testing.dto.ViaDTO;
//import com.testing.entities.Agent;
//import com.testing.entities.Via;
//import com.testing.mapper.AgentRowMapper;
//import com.testing.mapper.ViaRowMapper;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.boot.test.web.client.TestRestTemplate;
//import org.springframework.http.HttpEntity;
//import org.springframework.http.ResponseEntity;
//import org.springframework.jdbc.core.BeanPropertyRowMapper;
//import org.springframework.jdbc.core.JdbcTemplate;
//import org.springframework.test.context.jdbc.Sql;
//import java.util.Arrays;
//import java.util.List;
//import static org.junit.jupiter.api.Assertions.*;
//
//@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
//class AgentControllerTest {
//
//    @Autowired
//    private TestRestTemplate testRestTemplate;
//
//    @Autowired
//    private JdbcTemplate jdbcTemplate;
//
//    @Test
//    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "/test.sql")
//    @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = "/testDelete.sql")
//    void updateVia() {
//        String sql = "Select * from agent where id = ?";
//        AgentDTO agent = jdbcTemplate.queryForObject(sql,new AgentRowMapper(jdbcTemplate), 10012);
//        Via via = new Via();
//        via.setId(10016l);
//        agent.setVia(via);
//        HttpEntity<AgentDTO> request = new HttpEntity<>(agent);
//        testRestTemplate.put("/api/agent/asignar/"+agent.getId(), request);
//        AgentDTO agentUpdate = jdbcTemplate.queryForObject("select * from agent where id = ? and via = ?",new AgentRowMapper(jdbcTemplate), agent.getId(), agent.getVia().getId());
//        assertNotEquals(agent,agentUpdate);
//    }
//
//    @Test
//    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "/test.sql")
//    @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = "/testDelete.sql")
//    void getAll() {
//        AgentDTO[] agents = testRestTemplate.getForObject("/api/agent", AgentDTO[].class);
//        List<AgentDTO> agentList = Arrays.asList(agents);
//        assertNotNull(agentList);
//        String sql = "Select * from agent";
//        List<AgentDTO> Agent = jdbcTemplate.query(sql,new AgentRowMapper(jdbcTemplate));
//        for(int i = 0; i < Agent.size(); i++) {
//            assertEquals(Agent.get(i).getId(),agentList.get(i).getId());
//        }
//    }
//
//    @Test
//    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "/test.sql")
//    @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = "/testDelete.sql")
//    void getOne() {
//        AgentDTO agentDTO = testRestTemplate.getForObject("/api/agent/10012",AgentDTO.class);
//        assertNotNull(agentDTO);
//        String sql = "Select * from agent where id = ?";
//        AgentDTO agent = jdbcTemplate.queryForObject(sql,new AgentRowMapper(jdbcTemplate), agentDTO.getId());
//        assertEquals(agentDTO,agent);
//    }
//
//    @Test
//    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "/test.sql")
//    @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = "/testDelete.sql")
//    void getOneByCode() {
//        AgentDTO agentDTO = testRestTemplate.getForObject("/api/agent/code/PR001",AgentDTO.class);
//        assertNotNull(agentDTO);
//        String sql = "Select * from agent where codigo = ?";
//        AgentDTO agent = jdbcTemplate.queryForObject(sql,new AgentRowMapper(jdbcTemplate), agentDTO.getCode());
//        assertEquals(agentDTO,agent);
//    }
//
//    @Test
//    @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = "/testDelete.sql")
//    void save() {
//        AgentDTO agent = new AgentDTO();
//        agent.setCode("RF13");
//        agent.setName("RICARDO");
//        agent.setLastname("LOPEZ");
//        agent.setCodeAgent("XCZ123");
//        agent.setYears(30.50f);
//        agent.setVia(null);
//        HttpEntity<AgentDTO> request = new HttpEntity<>(agent);
//        ResponseEntity<AgentDTO> agentResponseEntity = testRestTemplate.postForEntity("/api/agent", request , AgentDTO.class);
//        assertNotNull(agentResponseEntity.getBody().getId());
//        String sql = "Select * from agent where id = ? or codigo = ?";
//        assertNotNull(jdbcTemplate.queryForObject(sql, new AgentRowMapper(jdbcTemplate),agentResponseEntity.getBody().getId(), agentResponseEntity.getBody().getCode()));
//        assertEquals("RF13", agentResponseEntity.getBody().getCode());
//        assertEquals("RICARDO", agentResponseEntity.getBody().getName());
//        assertEquals("LOPEZ", agentResponseEntity.getBody().getLastname());
//        assertEquals("XCZ123", agentResponseEntity.getBody().getCodeAgent());
//        assertEquals(30.50, agentResponseEntity.getBody().getYears());
//        assertEquals(null, agentResponseEntity.getBody().getVia());
//    }
//
//    @Test
//    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "/test.sql")
//    @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = "/testDelete.sql")
//    void update() {
//        AgentDTO agentDTO = jdbcTemplate.queryForObject("select * from agent where codigo = ?", new AgentRowMapper(jdbcTemplate), "PR001");
//        agentDTO.setName("DIEGO");
//        HttpEntity<AgentDTO> request = new HttpEntity<>(agentDTO);
//        testRestTemplate.put("/api/agent/"+agentDTO.getId(), request);
//        agentDTO = jdbcTemplate.queryForObject("select * from agent where codigo = ?", new AgentRowMapper(jdbcTemplate), "PR001");
//        assertNotEquals("JUAN",agentDTO.getName());
//    }
//
//    @Test
//    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "/test.sql")
//    @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = "/testDelete.sql")
//    void delete() {
//        AgentDTO agent = jdbcTemplate.queryForObject("select * from agent where codigo = ?", new AgentRowMapper(jdbcTemplate), "PR001");
//        String url = "/api/agent/"+agent.getId();
//        testRestTemplate.delete(url);
//        assertNotEquals(agent,jdbcTemplate.query("select * from agent where codigo = ?", new AgentRowMapper(jdbcTemplate), "PR001"));
//    }
//}