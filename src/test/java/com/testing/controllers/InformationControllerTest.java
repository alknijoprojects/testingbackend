//package com.testing.controllers;
//
//import com.testing.dto.AgentDTO;
//import com.testing.dto.InformationDTO;
//import com.testing.entities.Information;
//import com.testing.entities.Via;
//import com.testing.mapper.AgentRowMapper;
//import com.testing.mapper.InformationRowMapper;
//import org.junit.jupiter.api.Test;
//import org.modelmapper.ModelMapper;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.boot.test.web.client.TestRestTemplate;
//import org.springframework.http.HttpEntity;
//import org.springframework.jdbc.core.BeanPropertyRowMapper;
//import org.springframework.jdbc.core.JdbcTemplate;
//import org.springframework.test.context.jdbc.Sql;
//
//import java.util.Arrays;
//import java.util.List;
//
//import static org.junit.jupiter.api.Assertions.*;
//
//@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
//class InformationControllerTest {
//
//    @Autowired
//    private TestRestTemplate testRestTemplate;
//
//    @Autowired
//    private JdbcTemplate jdbcTemplate;
//
//    @Test
//    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "/informationTest.sql")
//    @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = "/testDelete.sql")
//    void getAll() {
//        InformationDTO[] informationDTOS = testRestTemplate.getForObject("/api/Information", InformationDTO[].class);
//        List<InformationDTO> informationDTOList = Arrays.asList(informationDTOS);
//        assertNotNull(informationDTOList);
//        String sql = "Select * from information";
//        List<InformationDTO> information = jdbcTemplate.query(sql, new InformationRowMapper(jdbcTemplate));
//        for(int i = 0; i < information.size(); i++) {
//            assertEquals(information.get(i).getId(),informationDTOList.get(i).getId());
//        }
//    }
//
//    @Test
//    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "/informationTest.sql")
//    @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = "/testDelete.sql")
//    void getOne() {
//        InformationDTO informationDTO = testRestTemplate.getForObject("/api/Information/50004", InformationDTO.class);
//        assertNotNull(informationDTO);
//        String sql = "Select * from information where id = ?";
//        InformationDTO information = jdbcTemplate.queryForObject(sql,new InformationRowMapper(jdbcTemplate), informationDTO.getId());
//        assertEquals(informationDTO,information);
//    }
//
//    @Test
//    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "/informationTest.sql")
//    @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = "/testDelete.sql")
//    void delete() {
//        InformationDTO informationDTO = testRestTemplate.getForObject("/api/Information/50004",InformationDTO.class);
//        String url = "/api/Information/"+informationDTO.getId();
//        testRestTemplate.delete(url);
//        informationDTO= testRestTemplate.getForObject("/api/Information/50004",InformationDTO.class);
//        assertNull(informationDTO);
//    }
//
//    @Test
//    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "/informationTest.sql")
//    @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = "/testDelete.sql")
//    void search() {
//        InformationDTO[] informationDTOS = testRestTemplate.getForObject("/api/Information/search?filter=PR001",InformationDTO[].class);
//        List<InformationDTO> informationDTOList = Arrays.asList(informationDTOS);
//        assertNotNull(informationDTOList);
//        List<InformationDTO> information = jdbcTemplate.query("select * from information where cagente = ?",new InformationRowMapper(jdbcTemplate), "PR001");
//        assertNotNull(information);
//        assertEquals(informationDTOList,information);
//    }
//}