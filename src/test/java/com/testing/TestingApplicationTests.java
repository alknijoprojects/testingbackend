package com.testing;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
class TestingApplicationTests {

    @Test
    void contextLoads() {
        assertNotNull(TestingApplication.class);
    }
}
