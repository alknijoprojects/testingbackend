package com.testing.mapper;

import com.testing.dto.AgentDTO;
import com.testing.dto.InformationDTO;
import com.testing.dto.ViaDTO;
import com.testing.entities.Via;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;


public class InformationRowMapper implements RowMapper<InformationDTO>  {

    private JdbcTemplate jdbcTemplate;

    public InformationRowMapper(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public InformationDTO mapRow(ResultSet rs, int i )throws SQLException {
        InformationDTO informationDTO = new InformationDTO();
        informationDTO.setId(rs.getLong(1));
        informationDTO.setCodeAgent(rs.getString("cagente"));
        informationDTO.setDirection(rs.getString("direccion"));
        informationDTO.setNameAgent(rs.getString("nagente"));
        informationDTO.setVia(extractVia(rs.getLong("via")));
        return informationDTO;
    }
    public Via extractVia(Long id){
        Via via =  jdbcTemplate.queryForObject("Select * from via where id = ?",new ViaEntityMapper(), id);
        return via;
    }
}
