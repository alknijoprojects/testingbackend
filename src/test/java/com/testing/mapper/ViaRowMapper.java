package com.testing.mapper;

import com.testing.dto.ViaDTO;
import com.testing.entities.Via;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ViaRowMapper implements RowMapper<ViaDTO> {

    @Override
    public ViaDTO mapRow(ResultSet rs, int i )throws SQLException {
        ViaDTO via = new ViaDTO();
        via.setId(rs.getLong("id"));
        via.setType(rs.getString("tipo"));
        via.setSense(rs.getString("sentido"));
        via.setCongestionNumber(rs.getFloat("ncongestion"));
        via.setNumber(rs.getInt("numero"));
        return via;
    }
}
