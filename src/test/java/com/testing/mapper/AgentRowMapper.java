package com.testing.mapper;

import com.testing.dto.AgentDTO;
import com.testing.entities.Via;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class AgentRowMapper implements RowMapper<AgentDTO> {

    public AgentRowMapper(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    private JdbcTemplate jdbcTemplate;


    @Override
    public AgentDTO mapRow(ResultSet rs, int i )throws SQLException {
        AgentDTO agentDTO = new AgentDTO();
        agentDTO.setId(rs.getLong(1));
        agentDTO.setName(rs.getString("nombre"));
        agentDTO.setCodeAgent(rs.getString("cdetransito"));
        agentDTO.setCode(rs.getString("codigo"));
        agentDTO.setLastname(rs.getString("apellido"));
        agentDTO.setYears(rs.getFloat("anose"));
        agentDTO.setVia(extractVia(rs.getLong("via")));
        return agentDTO;
    }
    public Via extractVia(Long id){
        if(id==0){
            return null;
        }
        Via via =  jdbcTemplate.queryForObject("Select * from via where id = ?",new ViaEntityMapper(), id);
        return via;
    }
}
