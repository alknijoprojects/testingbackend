package com.testing.controllers;

import com.testing.dto.BaseDTO;
import com.testing.entities.Base;
import com.testing.exceptions.EntityNotFoundException;
import com.testing.exceptions.NotFoundQueryException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import java.io.Serializable;

public interface BaseController <E extends Base, D extends BaseDTO, I extends Serializable> {

	 ResponseEntity<D> getOne(@PathVariable I i) throws  NotFoundQueryException;
	 ResponseEntity<D> save(@RequestBody D d) throws EntityNotFoundException, NotFoundQueryException;
	 ResponseEntity<D> update(@PathVariable I i, @RequestBody D d) throws EntityNotFoundException, NotFoundQueryException;
	 ResponseEntity<Boolean> delete(@PathVariable I i)throws EntityNotFoundException;
}
