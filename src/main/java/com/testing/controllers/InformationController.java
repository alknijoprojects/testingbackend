package com.testing.controllers;

import com.testing.config.TestingConfiguration;
import com.testing.dto.InformationDTO;
import com.testing.exceptions.EntityNotFoundException;
import com.testing.exceptions.NotFoundQueryException;
import com.testing.services.InformationService;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import com.testing.controllers.impl.BaseControllerImpl;
import com.testing.entities.Information;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path= "api/Information")
public class InformationController extends BaseControllerImpl<Information, InformationDTO, InformationService> {

    private final ModelMapper modelMapper;

    private TestingConfiguration testingConfiguration;

    public InformationController(InformationService service, ModelMapper modelMapper, TestingConfiguration testingConfiguration) {
        super(service);
        this.modelMapper = modelMapper;
        this.testingConfiguration = testingConfiguration;
    }

    @Override
    public ResponseEntity<List<InformationDTO>> getAll() throws EntityNotFoundException {
        List<Information> x = service.findAll();
        List<InformationDTO> z =  testingConfiguration.mapAll(x,InformationDTO.class);
        return ResponseEntity.status(HttpStatus.OK).body(z);
    }

    @Override
    public ResponseEntity<InformationDTO> getOne(Long id) throws NotFoundQueryException {
        Information information = service.findById(id);
        InformationDTO informationDTO = null;
        if(information==null){
            return ResponseEntity.status(HttpStatus.OK).body(informationDTO);
        }
        informationDTO = modelMapper.map(information,InformationDTO.class);
        return ResponseEntity.status(HttpStatus.OK).body(informationDTO);
    }

    @Override
    public ResponseEntity<InformationDTO> save(InformationDTO entity) throws EntityNotFoundException {
        Information information = service.save(entity);
        entity = modelMapper.map(entity, InformationDTO.class);
        return ResponseEntity.status(HttpStatus.OK).body(entity);
    }

    @Override
    public ResponseEntity<InformationDTO> update(Long id, InformationDTO entity) throws EntityNotFoundException {
        return ResponseEntity.status(HttpStatus.OK).body(entity);
    }

    @Override
    public ResponseEntity<Boolean> delete(Long id) throws EntityNotFoundException {
        return ResponseEntity.status(HttpStatus.NO_CONTENT).body(service.delete(id));
    }

    @GetMapping("/search")
    public ResponseEntity<List<InformationDTO>> search(@RequestParam String filter) throws NotFoundQueryException {
        List<Information> information = service.searchByCode(filter);
        List<InformationDTO> informationDTOS = testingConfiguration.mapAll(information, InformationDTO.class);
        return ResponseEntity.status(HttpStatus.OK).body(informationDTOS);
    }
}
