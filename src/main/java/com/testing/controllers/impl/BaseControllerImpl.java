package com.testing.controllers.impl;

import com.testing.controllers.BaseController;
import com.testing.dto.BaseDTO;
import com.testing.entities.Base;
import com.testing.exceptions.EntityNotFoundException;
import com.testing.exceptions.NotFoundQueryException;
import com.testing.services.BaseService;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

public abstract class BaseControllerImpl<E extends Base, D extends BaseDTO, S extends BaseService<E, Long>>implements BaseController<E, D, Long> {

	protected S service;

	protected BaseControllerImpl(S service) {
		this.service = service;
	}

	@ApiOperation("Muestra una lista")
	@GetMapping("")
	public abstract ResponseEntity<List<D>> getAll() throws EntityNotFoundException;

	@ApiIgnore
	@GetMapping("/{id}")
	public abstract ResponseEntity<D> getOne(@PathVariable Long id) throws NotFoundQueryException;

	@PreAuthorize("hasRole('ADMIN')")
	@PostMapping
	public abstract ResponseEntity<D> save(@RequestBody D d) throws EntityNotFoundException, NotFoundQueryException;

	@PreAuthorize("hasRole('ADMIN')")
	@PutMapping("/{id}")
	public abstract ResponseEntity<D> update(@PathVariable Long id, @RequestBody D d) throws EntityNotFoundException, NotFoundQueryException;

	@PreAuthorize("hasRole('ADMIN')")
	@DeleteMapping("/{id}")
	public abstract ResponseEntity<Boolean> delete(@PathVariable Long id) throws EntityNotFoundException;

}
