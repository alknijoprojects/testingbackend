package com.testing.controllers;

import com.testing.config.TestingConfiguration;
import com.testing.dto.AgentDTO;
import com.testing.entities.Agent;
import com.testing.exceptions.EntityNotFoundException;
import com.testing.exceptions.LimitViaAssignAgentException;
import com.testing.exceptions.LowCongestionException;
import com.testing.exceptions.NotFoundQueryException;
import com.testing.services.AgentService;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.testing.controllers.impl.BaseControllerImpl;

import java.util.List;


@RestController
@RequestMapping(path= "api/agent")
public class AgentController extends BaseControllerImpl<Agent, AgentDTO, AgentService> {

    private final ModelMapper modelMapper;

    private TestingConfiguration testingConfiguration;

    public AgentController(AgentService service, ModelMapper modelMapper, TestingConfiguration testingConfiguration) {
        super(service);
        this.modelMapper = modelMapper;
        this.testingConfiguration = testingConfiguration;
    }

    @PutMapping("/asignar/{id}")
    public ResponseEntity<AgentDTO> updateVia(@PathVariable Long id, @RequestBody AgentDTO agentDTO)  {

        try {
            Agent agentEntity = modelMapper.map(agentDTO, Agent.class);
            agentEntity = service.assignViaInTheAgent(id,agentEntity);
            AgentDTO agent = modelMapper.map(agentEntity,AgentDTO.class);
            return ResponseEntity.status(HttpStatus.OK).body(agent);
        } catch (NotFoundQueryException e) {
            return ResponseEntity.status(401).body(null);
        } catch (LowCongestionException e) {
            return ResponseEntity.status(403).body(null);
        } catch (LimitViaAssignAgentException e) {
            return  ResponseEntity.status(402).body(null);
        }


    }

    @Override
    public ResponseEntity<List<AgentDTO>>getAll() throws EntityNotFoundException {
        List<Agent> x = service.findAll();
        List<AgentDTO> z =  testingConfiguration.mapAll(x,AgentDTO.class);
        return new ResponseEntity<>(z,HttpStatus.OK);
    }

    @Override
    public ResponseEntity<AgentDTO> getOne(Long id) throws NotFoundQueryException {
        Agent agentEntity = service.findById(id);
        AgentDTO agentDTO = modelMapper.map(agentEntity,AgentDTO.class);
        return ResponseEntity.status(HttpStatus.OK).body(agentDTO);
    }

    @GetMapping("/code/{code}")
    public ResponseEntity<AgentDTO> getOneByCode(@PathVariable String code) throws NotFoundQueryException {
        Agent agentEntity = service.findByCode(code);
        AgentDTO agentDTO = null;
        if(agentEntity!=null){
            agentDTO = modelMapper.map(agentEntity,AgentDTO.class);
        }
        return ResponseEntity.status(HttpStatus.OK).body(agentDTO);
    }

    @Override
    public ResponseEntity<AgentDTO> save(AgentDTO agentDTO) throws EntityNotFoundException,NotFoundQueryException {
            Agent agentEntity = modelMapper.map(agentDTO, Agent.class);
            agentEntity = service.createAgent(agentEntity);
            agentDTO = modelMapper.map(agentEntity,AgentDTO.class);
            return ResponseEntity.status(HttpStatus.OK).body(agentDTO);
    }

    @Override
    public ResponseEntity<AgentDTO> update(Long id, AgentDTO entity) throws NotFoundQueryException {
        Agent agentEntity = modelMapper.map(entity, Agent.class);
        agentEntity = service.update(id,agentEntity);
        AgentDTO agentDTO = modelMapper.map(agentEntity,AgentDTO.class);
        return ResponseEntity.status(HttpStatus.OK).body(agentDTO);
    }

    @Override
    public ResponseEntity<Boolean> delete(Long id) throws EntityNotFoundException {
        return ResponseEntity.status(HttpStatus.NO_CONTENT).body(service.verifyAndDelete(id));
    }
}
