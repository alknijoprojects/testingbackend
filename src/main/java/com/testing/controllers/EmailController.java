package com.testing.controllers;

import com.testing.services.impl.EmailServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EmailController {

    @Autowired
    EmailServiceImpl emailService;

    @GetMapping("/email/send")
    public ResponseEntity<?> sendEmail() {
        emailService.sendEmail();
        return new ResponseEntity("Correo enviado con exito", HttpStatus.OK);
    }

    @GetMapping("/email/send-html")
    public ResponseEntity<?> SendEmailTemplate() {
        emailService.SendEmailTemplate();
        return new ResponseEntity("Correo enviado con exito", HttpStatus.OK);
    }
}
