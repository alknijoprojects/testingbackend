package com.testing.controllers;

import com.testing.config.TestingConfiguration;
import com.testing.controllers.impl.BaseControllerImpl;
import com.testing.dto.ViaDTO;
import com.testing.entities.Via;
import com.testing.exceptions.EntityNotFoundException;
import com.testing.exceptions.NotFoundQueryException;
import com.testing.services.ViaService;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path= "api/Via")
public class ViaController extends BaseControllerImpl<Via, ViaDTO, ViaService> {

    private final ModelMapper modelMapper;

    private TestingConfiguration testingConfiguration;

    public ViaController(ViaService service, ModelMapper modelMapper) {
        super(service);
        this.modelMapper = modelMapper;
    }

    @Override
    public ResponseEntity<List<ViaDTO>> getAll() throws EntityNotFoundException {
        List<Via> x = service.findAll();
        List<ViaDTO> z =  testingConfiguration.mapAll(x,ViaDTO.class);
        return new ResponseEntity<>(z,HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ViaDTO> getOne(Long id) throws NotFoundQueryException {
        Via via = service.findById(id);
        ViaDTO viaDTO =  modelMapper.map(via,ViaDTO.class);
        return new ResponseEntity<>(viaDTO,HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ViaDTO> save(ViaDTO via) throws NotFoundQueryException {
        Via v = service.save(via);
        ViaDTO viaDTO =  modelMapper.map(v,ViaDTO.class);
        return new ResponseEntity<>(viaDTO,HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ViaDTO> update(Long id, ViaDTO via) throws EntityNotFoundException {
        Via v = service.update(id,via);
        ViaDTO viaDTO =  modelMapper.map(v,ViaDTO.class);
        return new ResponseEntity<>(viaDTO,HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Boolean> delete(Long id) throws EntityNotFoundException {
         return new ResponseEntity<>(service.delete(id),HttpStatus.NO_CONTENT);
    }
}
