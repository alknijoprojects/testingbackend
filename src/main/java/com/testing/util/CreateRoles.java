//package com.testing.util;
//
//import com.testing.security.entities.Role;
//import com.testing.security.enums.RoleName;
//import com.testing.security.service.impl.RolServiceImpl;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.CommandLineRunner;
//import org.springframework.stereotype.Component;
//
//@Component
//public class CreateRoles implements CommandLineRunner {
//
//    @Autowired
//    RolServiceImpl rolService;
//    @Override
//    public void run(String... args) throws Exception {
//        Role rolAdmin = new Role(RoleName.ROLE_ADMIN);
//        Role roleUser = new Role(RoleName.ROLE_USER);
//        rolService.save(rolAdmin);
//        rolService.save(roleUser);
//    }
//}
