package com.testing.services;

import com.testing.dto.ViaDTO;
import com.testing.entities.Via;
import com.testing.exceptions.EntityNotFoundException;
import com.testing.exceptions.NotFoundQueryException;

import java.util.List;

public interface ViaService extends BaseService<Via, Long> {

    /**
     * Busca todas las vias registradas en la base de datos.
     *
     * @return Devuelve todas las vias existentes en la base de datos.
     * @throws EntityNotFoundException Se generá una excepción si hay un error de conexión con la base de datos.
     */
    List<Via> findAll() throws EntityNotFoundException;

    /**
     * Busca a una via por su identificación 'id' en la base de datos.
     *
     * @param id Corresponde al identificador de la via que se va a buscar y encontrar.
     * @return Todos los datos de la via filtrado por su identificación 'id'.
     * @throws NotFoundQueryException Generará una excepción cuando en la consulta no se encuentre la via.
     */
    Via findById(Long id) throws NotFoundQueryException;

    /**
     * Guardara la via en la base de datos.
     *
     * @param entity Corresponde a la información de la via que se va a guardar.
     * @return Retornara la via registrada y un HTTP STATUS 200.
     * @throws EntityNotFoundException En caso de ingresar los valores distintos a los indicados se generá una
     *                                 excepción.
     */
    Via save(ViaDTO entity) throws NotFoundQueryException;

    /**
     * Actualiza la información de una via identificada por 'id' que se encuentre en la base de datos.
     *
     * @param id Corresponde al identificador de la via que se va a actualizar
     * @param via Corresponde a la información de la via que se va a actualizar.
     * @return El agente actualizado.
     * @throws EntityNotFoundException En caso de que no se encuentre el agente por el id indicado entonces se generá
     *                                 esta excepción.
     */
    Via update(Long id, ViaDTO via) throws EntityNotFoundException;

    /**
     * Eliminara una via por medio de su identificador 'id'.
     *
     * @param id Corresponde al identificador de la via que se va a eliminar.
     * @return Retornara un True cuando encuentre la via por su identificador 'id' y la elimine.
     *         de la base de datos.
     * @throws EntityNotFoundException En caso de no encontrar ninguna via por el id indicado retornara un false y
     *                                 se generá una excepción.
     */
    boolean delete(Long id) throws EntityNotFoundException;
}
