package com.testing.services;

import com.testing.dto.InformationDTO;
import com.testing.entities.Information;
import com.testing.exceptions.EntityNotFoundException;
import com.testing.exceptions.NotFoundQueryException;

import java.util.List;

public interface InformationService extends BaseService<Information, Long> {

	/**
	 * Hace el conteo de las vias asignadas al agente para validar que solo puede tener 3 vias asignadas.
	 *
	 * @param filter Corresponde al Codigo del Agente por el cual se filtrara
	 * @return Una variable entero en la cual esta almacenado por la consulta generada.
	 * @throws NotFoundQueryException En caso de no encontrar el codigo del agente indicado se generá la excepción.
	 */
	int countById(String filter) throws NotFoundQueryException;

	/**
	 * Filtrara en la tabla información por codigo del agente o dirección de la via.
	 *
	 * @param filter Corresponde al Codigo del agente o Dirección de la via pro el cual se filtrara.
	 * @return Retornara el agente con las vias que le han sido asignadas o retornara las vias con la cantidad de agentes
	 * que se le han asignado.
	 * @throws NotFoundQueryException En caso de no encontrar el codigo del agente o la dirección de la via registrada en la
	 * tabla information.
	 */
	List<Information> searchByCode(String filter) throws NotFoundQueryException;

	/**
	 * Busca toda la información registradas en la base de datos.
	 *
	 * @return Devuelve toda la información existente en la base de datos.
	 * @throws EntityNotFoundException Se generá una excepción si hay un error de conexión con la base de datos.
	 */
	List<Information> findAll() throws EntityNotFoundException;

	/**
	 * Busca la información por su identificación 'id' en la base de datos.
	 *
	 * @param id Corresponde al identificador de la información que se va a buscar y encontrar.
	 * @return Todos los datos de la información filtrado por su identificación 'id'.
	 * @throws NotFoundQueryException Generará una excepción cuando en la consulta no se encuentre la información.
	 */
	Information findById(Long id) throws NotFoundQueryException;

	/**
	 * Guardara la información en la base de datos.
	 *
	 * @param information Corresponde a los datos a ingresar en la tabla información.
	 * @return Retornara la información registrada y un HTTP STATUS 200.
	 * @throws EntityNotFoundException En caso de ingresar los valores distintos a los indicados se generá una
	 *                                 excepción.
	 */
	Information save(InformationDTO information) throws EntityNotFoundException;

	/**
	 * Actualiza la información de una via identificada por 'id' que se encuentre en la base de datos.
	 *
	 * @param id Corresponde al identificador de la información que se va a actualizar
	 * @param information Corresponde a la información de la via que se va a actualizar.
	 * @return La información actualizada.
	 * @throws EntityNotFoundException En caso de que no se encuentre la informacion a actualizar, esto generará una
	 * 								   excepción.
	 */
	Information update(Long id, InformationDTO information) throws EntityNotFoundException;

	/**
	 * Eliminara una columna que contiene la información del agente y las vias asignadas
	 * por medio de su identificador 'id'.
	 * @param id Corresponde al identificador de la información que se va a eliminar.
	 * @return Retornara un True cuando encuentre la información por su identificador 'id' y la elimine de la base de
	 * datos.
	 * @throws EntityNotFoundException En caso de no encontrar ninguna información por el id indicado retornara un
	 * 							 	   false y se generá una excepción.
	 */
	boolean delete(Long id) throws EntityNotFoundException;


}
