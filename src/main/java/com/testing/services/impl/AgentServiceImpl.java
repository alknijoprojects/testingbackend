package com.testing.services.impl;

import com.testing.entities.Agent;
import com.testing.entities.Information;
import com.testing.entities.Via;
import com.testing.exceptions.EntityNotFoundException;
import com.testing.exceptions.LimitViaAssignAgentException;
import com.testing.exceptions.LowCongestionException;
import com.testing.exceptions.NotFoundQueryException;
import com.testing.repositories.AgentRepository;
import com.testing.repositories.BaseRepository;
import com.testing.repositories.InformationRepository;
import com.testing.services.AgentService;
import com.testing.services.InformationService;
import com.testing.services.ViaService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AgentServiceImpl extends BaseServiceImpl<Agent, Long> implements AgentService {

	private AgentRepository agentRepository ;
	private final ViaService viaService;
	private final InformationService informationService;
	private final ModelMapper modelMapper;
	private final InformationRepository informationRepository;

	private static float ROADCONGESTION;
	@Value("${roadCongestion}")
	public void setRoadCongestion(float roadCongestion){
		ROADCONGESTION = roadCongestion;
	}

	public AgentServiceImpl(BaseRepository<Agent, Long> baseRepository, AgentRepository agentRepository, ViaService viaService, InformationService informationService, ModelMapper modelMapper, InformationRepository informationRepository) {
		super(baseRepository);
		this.agentRepository = agentRepository;
		this.viaService = viaService;
		this.informationService = informationService;
		this.modelMapper = modelMapper;
		this.informationRepository = informationRepository;
	}

	private boolean checkAgent(Agent agentDTO) throws NotFoundQueryException{
		Optional<Agent> agentFound = agentRepository.findByCode(agentDTO.getCode());
		return agentFound.isPresent();
	}

	@Override
	public Agent createAgent(Agent agentDTO) throws EntityNotFoundException, NotFoundQueryException{
		if(checkAgent(agentDTO)){
			throw new EntityNotFoundException("El usuario ya ha sido creado en la base de datos.!"+agentDTO.getId()+" con Codigo :"+agentDTO.getCode());
		}
		Agent agentEntity = modelMapper.map(agentDTO, Agent.class);
		return agentRepository.save(agentEntity);
	}

	@Override
	public Agent assignViaInTheAgent(Long id, Agent entity) throws NotFoundQueryException, LimitViaAssignAgentException, LowCongestionException {
		Information inf = new Information();
		String nom = entity.getName();
		Via via = viaService.findById(entity.getVia().getId());
		System.out.println("la via encontra tiene estos valores: "+via);
		if(via.getCongestionNumber()<ROADCONGESTION){
			throw new LowCongestionException("la congestion de via es inferior a 30");
		}
		entity.setVia(via);
		String street = via.getSense()+ via.getNumber();
		inf.setCodeAgent(entity.getCode());
		inf.setNameAgent(nom);
		inf.setDirection(street);
		inf.setVia(entity.getVia());
		if(informationService.countById(inf.getCodeAgent())>2){
			throw new LimitViaAssignAgentException("El agente ya se le han asignado 3 vias.");
		}
		informationRepository.save(inf);
		System.out.println("al parecer paso por el metodo assign erfecto "+ entity);
		return save(entity);
	}

	@Override
	public Agent findByCode(String code) throws NotFoundQueryException {
		Optional<Agent> entityOptional = agentRepository.findByCode(code);
		return entityOptional.orElse(null);
	}

	@Override
	public Agent save(Agent agentDTO) throws NotFoundQueryException {
		try {
			return agentRepository.save(agentDTO);
		}catch (Exception e ){
			throw new NotFoundQueryException("Sintax Error!"+e.getMessage());
		}
	}

	@Override
	public Agent update(Long id, Agent agentDTO) throws NotFoundQueryException{
		try {
			if(!agentRepository.findById(id).isPresent()){
				throw new NotFoundQueryException("Error en los datos");
			}
			Agent agentEntity = modelMapper.map(agentDTO, Agent.class);
			return baseRepository.save(agentEntity);
		}catch (Exception e){
			throw new NotFoundQueryException("Sixtax Error! User is not updated"+e.getMessage());
		}
	}

	@Override
	public boolean verifyAndDelete(Long id) throws EntityNotFoundException {
		try {
			if(!baseRepository.existsById(id)) {
				throw new EntityNotFoundException("El agente no fue encontrado");
			}
			Agent entityOptional = findById(id);
			if(entityOptional.getVia()==null) {
				baseRepository.deleteById(id);
				return true;
			}
			entityOptional.setVia(null);
			baseRepository.save(entityOptional);
			baseRepository.deleteById(id);
			return true;
		}catch (Exception e) {
			throw new EntityNotFoundException("Error al eliminar el Usuario"+e.getMessage());
		}

	}
}
