package com.testing.services.impl;

import com.testing.dto.ViaDTO;
import com.testing.entities.Via;
import com.testing.exceptions.EntityNotFoundException;
import com.testing.exceptions.NotFoundQueryException;
import com.testing.repositories.BaseRepository;
import com.testing.repositories.ViaRepository;
import com.testing.services.ViaService;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
public class ViaServiceImpl extends BaseServiceImpl<Via, Long> implements ViaService {

	private ModelMapper modelMapper;
	private ViaRepository viaRepository;

	public ViaServiceImpl(BaseRepository<Via, Long> baseRepository, ModelMapper modelMapper, ViaRepository viaRepository) {
		super(baseRepository);
		this.modelMapper = modelMapper;
		this.viaRepository = viaRepository;
	}

	@Override
	public Via save(ViaDTO via) throws NotFoundQueryException{
		try {
			Via viaEntity = modelMapper.map(via,Via.class);
			return viaRepository.save(viaEntity);
		}catch (Exception e){
			throw new NotFoundQueryException("Error al guardar la via"+e.getMessage());
		}
	}

	@Override
	public Via update(Long id, ViaDTO via) throws EntityNotFoundException {
		if(!viaRepository.findById(id).isPresent()) {
			throw new EntityNotFoundException("Error al ingresar la nueva via en la base de datos");
		}
		Via viaEntity = modelMapper.map(via,Via.class);
		return baseRepository.save(viaEntity);
	}
}
