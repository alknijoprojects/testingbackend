package com.testing.services.impl;

import com.testing.entities.Base;
import com.testing.exceptions.EntityNotFoundException;
import com.testing.exceptions.NotFoundQueryException;
import com.testing.repositories.BaseRepository;
import com.testing.services.BaseService;

import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.List;
import java.util.Optional;

public abstract class BaseServiceImpl<E extends Base, I extends Serializable> implements BaseService<E, I> {

    protected BaseRepository<E, I> baseRepository;

    protected BaseServiceImpl(BaseRepository<E, I> baseRepository) {
        this.baseRepository = baseRepository;
    }


    @Override
    @Transactional
    public List<E> findAll() throws EntityNotFoundException {
        try {
            return baseRepository.findAll();
        } catch (Exception e) {
            throw new EntityNotFoundException("Sintax Error!, User not exist!"+e.getMessage());
        }
    }

    @Override
    @Transactional
    public E findById(I id) throws NotFoundQueryException {
        try {
            Optional<E> entityOptional = baseRepository.findById(id);
            return entityOptional.orElse(null);
        } catch (Exception e) {
            throw new NotFoundQueryException("Sintax Error!, User not exist!"+e.getMessage());
        }
    }

    @Override
    @Transactional
    public boolean delete(I id) throws EntityNotFoundException {
        if(!baseRepository.existsById(id)) {
            throw new EntityNotFoundException("El agente no fue encontrado");
        }
        baseRepository.deleteById(id);
        return true;
    }
}




