package com.testing.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Service
public class EmailServiceImpl {

    @Autowired
    JavaMailSender javaMailSender;

    @Autowired
    TemplateEngine templateEngine;

    public void sendEmail() {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("danielalejandrogalviscala58@gmail.com");
        message.setTo("danielalejandrogalviscala58@gmail.com");
        message.setSubject("Probando email simple");
        message.setText("Esto es el contenido del email");

        javaMailSender.send(message);
    }

    public void SendEmailTemplate() {
        MimeMessage message = javaMailSender.createMimeMessage();
        try {
            MimeMessageHelper helper = new MimeMessageHelper (message, true);
            Context context = new Context();
            String htmlText = templateEngine.process("email-template", context);
            helper.setFrom("danielalejandrogalviscala58@gmail.com");
            helper.setTo("danielalejandrogalviscala58@gmail.com");
            helper.setSubject("Probando email simple");
            helper.setText(htmlText, true);

            javaMailSender.send(message);
        }catch (MessagingException e){
            e.printStackTrace();
        }
    }

}
