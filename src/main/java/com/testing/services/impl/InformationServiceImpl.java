package com.testing.services.impl;

import com.testing.dto.InformationDTO;
import com.testing.entities.Information;
import com.testing.exceptions.EntityNotFoundException;
import com.testing.exceptions.NotFoundQueryException;
import com.testing.repositories.BaseRepository;
import com.testing.repositories.InformationRepository;
import com.testing.services.InformationService;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class InformationServiceImpl extends BaseServiceImpl<Information, Long> implements InformationService {

	private InformationRepository informationRepository;
	private ModelMapper modelMapper;

	public InformationServiceImpl(BaseRepository<Information, Long> baseRepository, InformationRepository informationRepository, ModelMapper modelMapper) {
		super(baseRepository);
		this.informationRepository = informationRepository;
		this.modelMapper = modelMapper;
	}

	@Override
	public Information save(InformationDTO information) throws EntityNotFoundException {
		try {
			Information informationEntity = modelMapper.map(information,Information.class);
			informationEntity = informationRepository.save(informationEntity);
			return informationEntity;
		}catch (Exception e ){
			throw new EntityNotFoundException("Sintax Error!"+e.getMessage());
		}
	}

	@Override
	public Information update(Long id, InformationDTO information) throws EntityNotFoundException {
		try {
			Optional<Information> viaDTOptionalVia = informationRepository.findById(id);
			Information informationUpdate;
			if(viaDTOptionalVia.isPresent()){
				Information informationEntity = modelMapper.map(information,Information.class);
				informationUpdate = baseRepository.save(informationEntity);
				return informationUpdate;
			}
			throw new EntityNotFoundException("Error en los datos");
		}catch (Exception e){
			throw new EntityNotFoundException("Sixtax Error! Via is not updated"+e.getMessage());
		}
	}

	@Override
	public int countById(String filter) throws NotFoundQueryException {
		return informationRepository.countById(filter);
	}

	@Override
	public List<Information> searchByCode(String filter) throws NotFoundQueryException {
		return informationRepository.searchByCode(filter);
	}
}
