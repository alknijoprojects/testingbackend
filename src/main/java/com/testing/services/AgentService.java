package com.testing.services;

import com.testing.entities.Agent;
import com.testing.exceptions.EntityNotFoundException;
import com.testing.exceptions.LimitViaAssignAgentException;
import com.testing.exceptions.LowCongestionException;
import com.testing.exceptions.NotFoundQueryException;

import javax.validation.Valid;
import java.util.List;

public interface AgentService extends BaseService<Agent, Long> {

    /**
     * Crea un agente dentro de la base de datos si el codigo del agente no ha sido registrado.
     *
     * @param agentDTO Corresponde a la información del agente que se va a crear.
     * @return El agente creado.
     * @throws EntityNotFoundException En caso de que el agente ya haya sido registrado en la base de datos.
     */
    Agent createAgent(@Valid Agent agentDTO) throws EntityNotFoundException, NotFoundQueryException;

    /**
     * Actualiza la información de un agente dentro de la base de datos.
     *
     * @param id Corresponde al identificcador del agente que se va a actualizar
     * @param agentDTO Corresponde a la información del agente que se va a actualizar.
     * @return El agente actualizado.
     * @throws NotFoundQueryException En caso de que no se encuentre el agente por el id indicado entonces se generá
     *                                 esta excepción.
     */
    Agent update(@Valid Long id, Agent agentDTO) throws NotFoundQueryException;

    /**
     * Actualiza la información de un agente dentro de la base de datos teniendo en cuenta que si una vía tiene nivel de
     * congestión inferior a 30 no permitiróa asgnarle la vía.......
     *
     * @param id Corresponde al identificcador del agente que se va a actualizar.
     * @param agentDTO Corresponde a la información del agente que se va a actualizar con la via asignada.
     * @return El agente actualizado con la vía asignada.
     * @throws NotFoundQueryException En caso de que no se encuentre el agente por el id indicado o la via no tenga el
     *                                 nivel de congestión requerido entonces se generá esta excepción.
     */
    Agent assignViaInTheAgent(@Valid Long id, Agent agentDTO) throws NotFoundQueryException, LowCongestionException, LimitViaAssignAgentException;

    /**
     * Busca todos los agentes registrados en la base de datos.
     *
     * @return Devuelve todos los agentes de la base de datos.
     * @throws EntityNotFoundException Se generá una excepción si hay un error de conexión con la base de datos.
     */
    List<Agent> findAll() throws EntityNotFoundException;

    /**
     * Busca a un agente por su identificación 'id' en la base de dato.
     *
     * @param id Corresponde al identificcador del agente que se va a buscar y encontrar.
     * @return Todos los datos del agente filtrado por su identificación 'id'.
     * @throws NotFoundQueryException Generará una excepción cuando en la consulta no se encuentre el agente.
     */
    Agent findById(Long id) throws NotFoundQueryException;

    /**
     * Busca a un agente por su codigo 'code' en la base de datos.
     *
     * @param code Corresponde al codigo del agente que se va a buscar y encontrar.
     * @return Todos los datos del agente filtrado por su codigo 'code'.
     * @throws NotFoundQueryException Generará una excepción cuando en la consulta no se encuentre el agente.
     */
    Agent findByCode(String code) throws NotFoundQueryException;

    /**
     * Guardara al agente en la base de datos.
     *
     * @param agentDTO Corresponde a la información del agente que se va a guardar.
     * @return El agente registrado y un HTTP STATUS 200.
     * @throws EntityNotFoundException En caso de ingresar los valores distintos a los indicados se generá una
     *                                 excepción.
     */
    Agent save(@Valid Agent agentDTO) throws NotFoundQueryException;

    /**
     * Eliminara un agente por medio de su identificador 'id'.
     *
     * @param id Corresponde al identificcador del agente que se va a buscar y encontrar.
     * @return Retornara un True cuando encuentre el agente por su identificador 'id' y lo elimine
     *         de la base de datos.
     * @throws EntityNotFoundException En caso de no encontrar ningun agente por el id indicado retornara un false y
     *                                 se generá una excepción.
     */
    boolean verifyAndDelete(Long id) throws EntityNotFoundException;
}
