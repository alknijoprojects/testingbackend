package com.testing.services;

import com.testing.entities.Base;
import com.testing.exceptions.EntityNotFoundException;
import com.testing.exceptions.NotFoundQueryException;

import java.io.Serializable;
import java.util.List;

public interface BaseService<E extends Base, I extends Serializable> {

	 List<E> findAll() throws EntityNotFoundException;
	 E findById(I id) throws NotFoundQueryException;
	 boolean delete(I id) throws EntityNotFoundException;

}
