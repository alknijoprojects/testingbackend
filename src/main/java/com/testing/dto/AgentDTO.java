package com.testing.dto;

import com.testing.entities.Via;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.validation.constraints.NotBlank;

@Data
@ToString
@EqualsAndHashCode
public class AgentDTO extends BaseDTO {

    @NotBlank
    private String code;
    private String name;
    private String lastname;
    private float years;
    private String codeAgent;
    private Via via;

}
