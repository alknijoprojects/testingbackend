package com.testing.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;


@Data
@EqualsAndHashCode

public class ViaDTO extends BaseDTO {

    private String type;
    private String sense;
    private int number;
    private float congestionNumber;
}
