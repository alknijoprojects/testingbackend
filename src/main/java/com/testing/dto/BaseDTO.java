package com.testing.dto;


import lombok.Data;

import javax.persistence.MappedSuperclass;
import java.io.Serializable;

@MappedSuperclass
@Data
public class BaseDTO implements Serializable {

    private Long id;
}
