package com.testing.dto;

import com.testing.entities.Via;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;


@Data
@ToString
@EqualsAndHashCode

public class InformationDTO extends BaseDTO {

    private String nameAgent;
    private String direction;
    private String codeAgent;
    private Via via;

}
