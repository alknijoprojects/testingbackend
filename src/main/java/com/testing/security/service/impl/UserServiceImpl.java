package com.testing.security.service.impl;

import com.testing.security.entities.User;
import com.testing.security.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;


@Service
@Transactional
public class UserServiceImpl {

    @Autowired
    private UserRepository userRepository;

    public Optional<User> getByUserName(String username){
        return userRepository.findByUsername(username);
    }

    public Optional<User> getByUserNameOrEmail(String nameOrEmail){
        return userRepository.findByUsernameOrEmail(nameOrEmail,nameOrEmail);
    }

//    public Optional<User> getByTokenPassword(String tokenPassword){
//        return userRepository.findByTokenPassword(tokenPassword);
//    }

    public boolean existsByUsername(String username){
        return userRepository.existsByUsername(username);
    }

    public boolean existsByEmail(String email){
        return userRepository.existsByEmail(email);
    }

    public void save(User user){
        userRepository.save(user);
    }
}
