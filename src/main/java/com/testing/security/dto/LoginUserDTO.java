package com.testing.security.dto;

import lombok.Getter;
import lombok.Setter;
import javax.validation.constraints.NotBlank;

@Getter
@Setter
public class LoginUserDTO {
    @NotBlank
    private String username;
    @NotBlank
    private String password;
}
