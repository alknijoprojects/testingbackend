package com.testing.security.repositories;

import com.testing.security.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByUsername(String username);
    Optional<User> findByUsernameOrEmail(String username, String Email);
    boolean existsByUsername(String username);
    boolean existsByEmail(String email);

}
