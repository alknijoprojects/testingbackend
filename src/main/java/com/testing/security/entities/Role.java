package com.testing.security.entities;

import com.testing.security.enums.RoleName;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "Role")
@EqualsAndHashCode
@Getter
@Setter
public class Role {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotNull
    @Enumerated(EnumType.STRING)
    private RoleName roleName;

    public Role() {

    }
    public Role(@NotNull RoleName roleName){
        this.roleName = roleName;
    }
}
