package com.testing.security.entities;

import com.testing.entities.Base;
import com.testing.security.entities.Role;
import lombok.*;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "Users")
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Getter
@Setter
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "firstName", length = 50)
    @NotNull
    private String firstName;
    @NotNull
    @Column(name = "username", length = 50, unique = true)
    private String username;
    @NotNull
    @Column(name = "lastName", length = 50)
    private String lastName;
    @NotNull
    @Column(name = "email", length = 200)
    private String email;
    @NotNull
    @Column(name = "password")
    private String password;

    @NotNull
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name="users_roles",
            joinColumns = @JoinColumn(
                    name= "user_id"),
            inverseJoinColumns = @JoinColumn(
                    name = "rol_id"
            )
    )
    private Set<Role> role= new HashSet<>();

    public User(String firstName, String username, String lastName, String email, String password) {
        this.id = id;
        this.firstName = firstName;
        this.username = username;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
    }


}
