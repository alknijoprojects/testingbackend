package com.testing.security.controllers;

import com.testing.dto.Message;
import com.testing.security.dto.JwtDTO;
import com.testing.security.dto.LoginUserDTO;
import com.testing.security.dto.UserDTO;
import com.testing.security.entities.Role;
import com.testing.security.entities.User;
import com.testing.security.enums.RoleName;
import com.testing.security.jwt.JwtProvider;
import com.testing.security.service.impl.RolServiceImpl;
import com.testing.security.service.impl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.ParseException;
import java.util.HashSet;
import java.util.Set;

@RestController
@RequestMapping("/auth")
@CrossOrigin
public class UserRegistrationController {

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserServiceImpl userService;

    @Autowired
    RolServiceImpl rolService;

    @Autowired
    JwtProvider jwtProvider;

    @PostMapping("/new")
    public ResponseEntity<?> newUser(@Valid @RequestBody UserDTO userDTO, BindingResult bindingResult) {
        System.out.println("entro al register user");
        System.out.println(userDTO.getFirstName());
        System.out.println(bindingResult);
        if(bindingResult.hasErrors())
            return new ResponseEntity(new Message("campos mal puestos o email invalido"), HttpStatus.BAD_REQUEST);
        if(userService.existsByUsername(userDTO.getUsername()))
                return new ResponseEntity(new Message("ese nombre ya existe"), HttpStatus.BAD_REQUEST);
        if(userService.existsByEmail(userDTO.getEmail()))
            return new ResponseEntity(new Message("El email ya fue creado!"), HttpStatus.BAD_REQUEST);
        User user = new User(userDTO.getFirstName(),userDTO.getLastName(),userDTO.getUsername(),userDTO.getEmail(),passwordEncoder.encode(userDTO.getPassword()));
        Set<Role> roles = new HashSet<>();
        roles.add(rolService.getByRoleName(RoleName.ROLE_USER).get());
        if(userDTO.getRoles().contains("admin"))
            roles.add(rolService.getByRoleName(RoleName.ROLE_ADMIN).get());
        user.setRole(roles);
        userService.save(user);
        return new ResponseEntity(new Message("Usuario Guardado!."), HttpStatus.CREATED);

    }
    @PostMapping("/login")
    public ResponseEntity<JwtDTO> login(@Valid @RequestBody LoginUserDTO loginUserDTO, BindingResult bindingResult){
        if (bindingResult.hasErrors())
            return new ResponseEntity(new Message("campos mal puestos"), HttpStatus.BAD_REQUEST);
        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginUserDTO.getUsername(), loginUserDTO.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtProvider.generateToken(authentication);
        JwtDTO jwtDTO = new JwtDTO(jwt);
        return new ResponseEntity(jwtDTO, HttpStatus.OK);
    }
    @PostMapping("/refresh")
    public ResponseEntity<JwtDTO> refresh(@RequestBody JwtDTO jwtDto) throws ParseException {
        String token = jwtProvider.refreshToken(jwtDto);
        JwtDTO jwt = new JwtDTO(token);
        return new ResponseEntity(jwt, HttpStatus.OK);
    }
}
