package com.testing.security.enums;

public enum RoleName {
    ROLE_ADMIN, ROLE_USER
}
