package com.testing.entities;

import lombok.*;
import org.hibernate.envers.Audited;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "Via")
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Getter
@Setter
@ToString
@Audited
public class Via extends Base {

    @Column(name = "tipo", length = 50)
    private String type;
    @Column(name = "sentido", length = 50)
    private String sense;
    @Column(name="numero",  length = 11)
    private int number;
    @Column(name="ncongestion",  columnDefinition ="DECIMAL(10,2) ")
    private float congestionNumber;

}
