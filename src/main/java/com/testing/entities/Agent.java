package com.testing.entities;

import lombok.*;
import org.hibernate.envers.Audited;

import javax.persistence.*;

@Entity
@Table(name="Agent")
@ToString
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Getter
@Setter
@Audited
public class Agent extends Base {

	@Column(name = "codigo", length = 50)
    private String code;
    @Column(name = "nombre", length = 50)
    private String name;
    @Column(name = "apellido", length = 50)
    private String lastname;
	@Column(name="anose",  columnDefinition ="DECIMAL(10,1)")
    private float years;
    @Column(name = "cdetransito", length = 50)
    private String codeAgent;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="via")
    private Via via;
    
}
