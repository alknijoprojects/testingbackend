package com.testing.entities;

import lombok.*;
import org.hibernate.envers.Audited;

import javax.persistence.*;

@Entity
@Table(name="Information")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@EqualsAndHashCode
@Setter
@ToString
@Audited
public class Information extends Base {

	@Column(name = "nagente", length = 50)
    private String nameAgent;
    @Column(name = "direccion", length = 50)
    private String direction;
    @Column(name="cagente",  length = 50)
    private String codeAgent;
    @OneToOne(cascade= CascadeType.ALL)
    @JoinColumn(name="via")
    private Via via;
}
