package com.testing.exceptions;

public class NotFoundQueryException extends Exception {

	/**
	 * Encargado de regresar un error en la consulta.
	 *
	 * @param message devolvera el error generado realizando la consulta.
	 */
	public NotFoundQueryException(String message) {
		
		super(message);
	}
	
    public NotFoundQueryException(String message, Throwable cause) {
    	
    	super(message, cause);
    }

    public NotFoundQueryException() {

    }
}
