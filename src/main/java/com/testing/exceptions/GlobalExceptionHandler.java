package com.testing.exceptions;


import lombok.extern.java.Log;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
@Log
public class GlobalExceptionHandler {

    public static final String INTERNAL_ERROR_CUSTOM_MESSAGE = "... whatever internal error happened ...";

    @ExceptionHandler(Exception.class) //checkForEveryException
    @ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR, reason = "Error")
    public void everyException(Exception e) {
        log.info(INTERNAL_ERROR_CUSTOM_MESSAGE);
    }

    /**
     * this method only catches our EntityNotFoundException
     */
    @ExceptionHandler(EntityNotFoundException.class)
    @ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "No coffee found")
    public void everyException(EntityNotFoundException e) {
        log.info(e.getMessage());
    }

    @ExceptionHandler(LowCongestionException.class)
    @ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "No coffee found")
    public Exception everyException(LowCongestionException e) {
        return e;
    }

}


