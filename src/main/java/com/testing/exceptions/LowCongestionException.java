package com.testing.exceptions;

public class LowCongestionException extends Exception{

    public LowCongestionException(String message) {
        super(message);
    }

    public LowCongestionException(String message, Throwable cause) {
        super(message, cause);
    }
}
