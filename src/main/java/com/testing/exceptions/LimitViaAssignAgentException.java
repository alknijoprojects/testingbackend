package com.testing.exceptions;

public class LimitViaAssignAgentException extends Exception{

    public LimitViaAssignAgentException(String message) {
        super(message);
    }

    public LimitViaAssignAgentException(String message, Throwable cause) {
        super(message, cause);
    }
}
