package com.testing.repositories;

import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.testing.entities.Information;

@Repository
public interface InformationRepository extends BaseRepository<Information, Long> {

    @Query(
            value= "SELECT Count(*) FROM Information WHERE cagente = :filter",
            nativeQuery = true
    )
    int countById(@Param("filter") String filter);

    @Query(
            value= "SELECT * FROM Information WHERE cagente LIKE %:filter% OR direccion LIKE %:filter%",
            nativeQuery = true
    )
    List<Information> searchByCode(@Param("filter") String filter);
}
