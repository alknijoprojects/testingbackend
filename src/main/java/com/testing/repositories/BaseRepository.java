package com.testing.repositories;

import java.io.Serializable;
import org.springframework.data.jpa.repository.JpaRepository;
import com.testing.entities.Base;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface BaseRepository<E extends Base, I    extends Serializable> extends JpaRepository<E, I> {

}
