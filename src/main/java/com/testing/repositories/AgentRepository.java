package com.testing.repositories;

import com.testing.exceptions.NotFoundQueryException;
import org.springframework.stereotype.Repository;
import com.testing.entities.Agent;
import java.util.Optional;

@Repository
public interface AgentRepository extends BaseRepository<Agent, Long> {

    Optional<Agent> findByCode(String code) throws NotFoundQueryException;

}
