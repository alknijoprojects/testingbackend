package com.testing.repositories;

import org.springframework.stereotype.Repository;
import com.testing.entities.Via;

@Repository
public interface ViaRepository extends BaseRepository<Via, Long> {
	
}
